#!/bin/bash

# TLDR
sudo npm install -g tldr
# EPR Epub reader
python3 -m pip install git+https://github.com/wustho/epr.git

# Preview markdown
python3 -m pip install grip
# Loadtest
sudo npm install -g loadtest
# Taskbook
sudo npm install --global taskbook

sudo apt-get install fzf

